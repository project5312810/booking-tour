import React from 'react'
import './Home.css'
import video from '../../assets/video.mp4'
import { MdLocationOn } from 'react-icons/md'
import { HiFilter } from 'react-icons/hi'
import { FiFacebook } from 'react-icons/fi'
import { AiOutlineInstagram, AiOutlineSkype } from 'react-icons/ai'
import { BsListTask } from 'react-icons/bs'
import { TbApps } from 'react-icons/tb'
const Home = () => {
    return (
        <section className='home'>
            <div className='overlay'></div>
            <video src={video} muted autoPlay loop type='video/mp4' ></video>
            <div className='home-content container'>
                <div className='text-div'>
                    <span className='small-text'>
                        Our Packages
                    </span>
                    <h1 className='home-title'>
                        Search your Holidays
                    </h1>
                </div>
                <div className='card-div grid'>
                    <div className='destination-input'>
                        <div className='flex'>
                            <label htmlFor='city'>
                                Search your destination:
                            </label>
                        </div>
                        <div className='input flex'>
                            <input type='text' placeholder='Enter name here...' />
                            <MdLocationOn className='icon-locatino' />
                        </div>
                    </div>
                    <div className='date-input'>
                        <div className='flex'>
                            <label htmlFor='date'>
                                Search your date:
                            </label>
                        </div>
                        <div className='input flex'>
                            <input type='date' />
                        </div>
                    </div>
                    <div className='price-input'>
                        <div className='label-total flex'>
                            <label htmlFor='price'>
                                Max price:
                            </label>
                            <h3 className='total'>$5000</h3>
                        </div>
                        <div className='input flex'>
                            <input type='range' max='5000' min='1000' />
                        </div>
                    </div>
                    <div className='search-options flex'>
                        <HiFilter className='icon-filter' />
                        <span>MORE FILTERS</span>
                    </div>
                </div>
                <div className='home-footer-icons flex'>
                    <div className='right-icons'>
                        <FiFacebook className='icon-footer' />
                        <AiOutlineInstagram className='icon-footer' />
                        <AiOutlineSkype className='icon-footer' />
                    </div>
                    <div className='left-icons'>
                        <BsListTask className='icon-footer' />
                        <TbApps className='icon-footer' />
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Home