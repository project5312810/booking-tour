import React from 'react'
import './Footer.css'
import video2 from '../../assets/video2.mp4'
import { FiChevronRight, FiSend } from 'react-icons/fi'
import { MdOutlineTravelExplore } from 'react-icons/md'
import { AiFillInstagram, AiFillYoutube, AiOutlineTwitter } from 'react-icons/ai'
import { FaTripadvisor } from 'react-icons/fa'
const Footer = () => {
    return (
        <section className='footer' >
            <div className='video-div'>
                <video src={video2} loop autoPlay muted type='video/mp4'></video>
            </div>
            <div className='sec-content container'>
                <div className='contact-div flex'>
                    <div className='text'>
                        <small>KEEP IN TOUCH</small>
                        <h2> Travel with us</h2>
                    </div>
                    <div className='input-div flex'>
                        <input type='text' placeholder='Enter Email Adress' />
                        <button className='btn flex' type='submit'>
                            SEND <FiSend className='icon' />
                        </button>
                    </div>
                </div>
                <div className='footer-card flex'>
                    <div className='footer-intro flex'>
                        <div className='logo-div'>
                            <a href='/' className='logo flex'>
                                <MdOutlineTravelExplore className='icon' />
                                Travel.
                            </a>
                        </div>
                        <div className='footer-paragraph'>
                            If you are looking for a different activity,
                            to connect with nature, but above all to learn and value the coffee industry,
                            this tour is designed for you. Combine a learning experience, Spa and Nature.
                        </div>
                        <div className='footer-socials'>
                            <AiFillYoutube className='icon' />
                            <AiFillInstagram className='icon' />
                            <AiOutlineTwitter className='icon' />
                            <FaTripadvisor className='icon' />
                        </div>
                    </div>
                    <div className='footer-links grid'>
                        <div className='link-group'>
                            <span className='group-title'>
                                OUR AGENCY
                            </span>
                            <li className='footer-list flex'>
                                <FiChevronRight className='icon' />
                                Services
                            </li>
                            <li className='footer-list flex'>
                                <FiChevronRight className='icon' />
                                Insurance
                            </li>
                            <li className='footer-list flex'>
                                <FiChevronRight className='icon' />
                                Agency
                            </li>
                            <li className='footer-list flex'>
                                <FiChevronRight className='icon' />
                                Tour
                            </li>
                            <li className='footer-list flex'>
                                <FiChevronRight className='icon' />
                                Payment
                            </li>
                        </div>
                        <div className='link-group'>
                            <span className='group-title'>
                                PARTNERS
                            </span>
                            <li className='footer-list flex'>
                                <FiChevronRight className='icon' />
                                Bookings
                            </li>
                            <li className='footer-list flex'>
                                <FiChevronRight className='icon' />
                                Rentcars
                            </li>
                            <li className='footer-list flex'>
                                <FiChevronRight className='icon' />
                                HostelWord
                            </li>
                            <li className='footer-list flex'>
                                <FiChevronRight className='icon' />
                                Trivago
                            </li>
                            <li className='footer-list flex'>
                                <FiChevronRight className='icon' />
                                TripAdvisor
                            </li>
                        </div>
                        <div className='link-group'>
                            <span className='group-title'>
                                LAST MINUTE
                            </span>
                            <li className='footer-list flex'>
                                <FiChevronRight className='icon' />
                                London
                            </li>
                            <li className='footer-list flex'>
                                <FiChevronRight className='icon' />
                                California
                            </li>
                            <li className='footer-list flex'>
                                <FiChevronRight className='icon' />
                                Indonesia
                            </li>
                            <li className='footer-list flex'>
                                <FiChevronRight className='icon' />
                                Europe
                            </li>
                            <li className='footer-list flex'>
                                <FiChevronRight className='icon' />
                                Oceania
                            </li>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    )
}

export default Footer