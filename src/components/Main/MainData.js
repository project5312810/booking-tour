import img1 from '../../assets/1.jpg'
import img2 from '../../assets/2.jpg'
import img3 from '../../assets/3.jpg'
import img4 from '../../assets/4.jpg'
import img5 from '../../assets/5.jpg'
import img6 from '../../assets/6.jpg'
import img7 from '../../assets/7.jpg'
import img8 from '../../assets/8.jpg'
import img9 from '../../assets/9.jpg'
export const MainData = [
    {
        id: 1,
        imgSrc: img1,
        destTitle: 'Bora Bora',
        location: 'New Zealend',
        grade: 'CULTURAL RELAX',
        fees: '$700',
        description: 'New Zealand Foreign Minister Nanaia Mahuta has expressed concern to China over any provision of lethal aid to support Russia in its war against Ukraine during a meeting with her Chinese counterpart'
    },
    {
        id: 2,
        imgSrc: img2,
        destTitle: 'Machu Picchu',
        location: 'Peru',
        grade: 'CULTURAL RELAX',
        fees: '$750',
        description: 'Travel to Lake Titicaca & Machu Picchu From Lima Small guided group tour & activities exploring Lima nearby ruins, Machu Picchu inca citadel, Route of the sun Cusco, Lake Titicaca: Uros floating island.'
    },
    {
        id: 3,
        imgSrc: img3,
        destTitle: 'Great Barrier Reef',
        location: 'Australia',
        grade: 'CULTURAL RELAX',
        fees: '$600',
        description: 'One of Australia’s most remarkable natural gifts. The reef contains an abundance of marine life and comprises of over 3000 individual reef systems and coral cays and literally hundreds of picturesque.'
    },
    {
        id: 4,
        imgSrc: img4,
        destTitle: 'Cappadocia',
        location: 'Turkey',
        grade: 'CULTURAL RELAX',
        fees: '$800',
        description: 'Discover Cappadocia’s Top Tourist Attractions You Can’t Find Anywhere Else But in Türkiye. Plan a Trip to One of The Best Places to Visit in the World And its Miraculous Nature. Remarkable Landscapes.'
    },
    {
        id: 5,
        imgSrc: img5,
        destTitle: 'Guanajuato',
        location: 'Mexico',
        grade: 'CULTURAL RELAX',
        fees: '$700',
        description: 'Founded by the Spanish in the early 16th century, became the world The town fine Baroque and neoclassical buildings, resulting from the prosperity, have influenced buildings throughout central Mexico.'
    },
    {
        id: 6,
        imgSrc: img6,
        destTitle: 'Cinque Terre',
        location: 'Italy',
        grade: 'CULTURAL RELAX',
        fees: '$750',
        description: 'Find discounts on the best Cinque Terre. Tripadvisor helps you spend less resulting from the prosperity,. Compare prices & save money with Tripadvisor® World largest travel website. Amazing Experiences.'
    },
    {
        id: 7,
        imgSrc: img7,
        destTitle: 'Angkor Wat',
        location: 'Combodia',
        grade: 'CULTURAL RELAX',
        fees: '$700',
        description: 'An immense mausoleum of white marble, built in Agra between 1631 and 1648 by order of the Mughal emperor Shah Jahan in memory of his favourite wife, the Taj Mahal is the jewel of Muslim art in India .'
    },
    {
        id: 8,
        imgSrc: img8,
        destTitle: 'Taj Mahal',
        location: 'India',
        grade: 'CULTURAL RELAX',
        fees: '$720',
        description: 'New Zealand Foreign Minister Nanaia Mahuta has expressed concern to China over any provision of lethal aid to support Russia in its war against Ukraine during a meeting with her Chinese counterpart'
    },
    {
        id: 9,
        imgSrc: img9,
        destTitle: 'Bali Island',
        location: 'Indonesia',
        grade: 'CULTURAL RELAX',
        fees: '$520',
        description: 'Best Bali Vacation Rentals from Your Favourite Sites. Find Your Dream Vacation Home Now! HomeToGo®: Easy Multi-Site Price Comparison. Best Deals. Numerous Trusted Partners. Low Prices. Most Properties.'
    },

];