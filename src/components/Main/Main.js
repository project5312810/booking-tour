import React from 'react'
import './Main.css'
import { MainData } from './MainData'
const Main = () => {
    return (
        <div className='main container section'>
            <div className='section-title'>
                <h3 className='title'>
                    Most visited destinations
                </h3>
            </div>
            <div className='section-content grid'>
                {
                    MainData.map((item, index) => {
                        return (
                            <div key={index} className='single-destination'>
                                <div className='image-div'>
                                    <img src={item.imgSrc} alt={item.destTitle} />
                                </div>
                                <div className='card-info'>
                                    <h4 className='destTitle'>
                                        {item.destTitle}
                                    </h4>
                                    <span className='continent flex'>
                                        <span className='name'>
                                            {item.location}
                                        </span>
                                    </span>
                                    <div className='fees flex'>
                                        <div className='grade'>
                                            <span>
                                                {item.grade}
                                                <small>+1</small>
                                            </span>
                                        </div>
                                        <div className='price'>
                                            <h5>{item.fees}</h5>
                                        </div>
                                    </div>
                                    <div className='decs'>
                                        <p>{item.description}</p>
                                    </div>
                                    <button className='btn flex'>
                                        DETAILS
                                    </button>
                                </div>

                            </div>
                        )
                    })
                }
            </div>

        </div>
    )
}

export default Main