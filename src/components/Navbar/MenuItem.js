
export const MenuItems = [
    {
        title: "Home",
        url: "/",
        name: "nav-links",
    },
    {
        title: "Shop",
        url: "/shop",
        name: "nav-links",
    },
    {
        title: "About",
        url: "/about",
        name: "nav-links",
    },
    {
        title: "Pages",
        url: "/pages",
        name: "nav-links",
    },
    {
        title: "News",
        url: "/news",
        name: "nav-links",
    },
    {
        title: "Contact",
        url: "/contact",
        name: "nav-links",
    },
    {
        title: "BOOK NOW",
        url: "/book-now",
        name: "nav-links-mobile",
    },

];