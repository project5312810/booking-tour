import React, { Component } from 'react'
import './Navbar.css'
import { MdTravelExplore } from 'react-icons/md'
import { FaBars, FaTimes } from 'react-icons/fa'
import { MenuItems } from './MenuItem'
import { Link } from 'react-router-dom'
class Navbar extends Component {
    state = { clicked: false }
    handleClick = () => {
        this.setState({ clicked: !this.state.clicked })
    }
    render() {
        return (
            <nav className='nav-items' >
                <h1>
                    <MdTravelExplore className='icon' />
                    Travel.
                </h1>
                <div className='menu-icons' onClick={this.handleClick}>
                    <i>{this.state.clicked ? <FaTimes /> : <FaBars />}</i>
                </div>
                <ul className={this.state.clicked ? "nav-menu active" : "nav-menu"}>
                    {MenuItems.map((item, index) => {
                        return (
                            <li key={index}>
                                <Link className={item.name} to={item.url}>
                                    {item.title}
                                </Link>
                            </li>
                        )
                    })}
                    <button>BOOK NOW</button>
                </ul>
            </nav >
        )
    }
}

export default Navbar